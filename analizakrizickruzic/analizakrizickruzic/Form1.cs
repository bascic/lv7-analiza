﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace analizakrizickruzic
{
    public partial class Form1 : Form
    {
        bool naredu = true;
        int br = 0;
        static String igrac1, igrac2;

        public Form1()
        {
            InitializeComponent();
        }

        private void nova_igra_Click(object sender, EventArgs e)
        {
            naredu = true;
            br = 0;
            label.Text = igrac1;

            A1.Enabled = true;
            A1.Text = "";
            A2.Enabled = true;
            A2.Text = "";
            A3.Enabled = true;
            A3.Text = "";
            B1.Enabled = true;
            B1.Text = "";
            B2.Enabled = true;
            B2.Text = "";
            B3.Enabled = true;
            B3.Text = "";
            C1.Enabled = true;
            C1.Text = "";
            C2.Enabled = true;
            C2.Text = "";
            C3.Enabled = true;
            C3.Text = "";

            textbox1.Clear();
            textbox2.Clear();

        }

        private void kraj_Click(object sender, EventArgs e)
        {





            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (naredu)
            {
                button.Text = "X";
                label.Text = igrac2;
            }
            else
            {
                button.Text = "O";
                label.Text = igrac1;
            }

            naredu = !naredu;
            button.Enabled = false;
            br++;
            Provjera();
        }
    

        public static void Postaviime(string prvi, string drugi)
        {
            igrac1 = prvi;
            igrac2 = drugi;
        }
        public void Provjera()
        {

            bool Pobjeda = false;
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled)) 
                Pobjeda = true;
            if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled)) 
                Pobjeda = true;
            if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled)) 

                Pobjeda = true;

            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled)) 
                Pobjeda = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled)) 
                Pobjeda = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled)) // provjera okomito

                Pobjeda = true;

            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled)) // provjerava dijagonalno
                Pobjeda = true;
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled)) // provjerava dijagonalno

                Pobjeda = true;


            if (Pobjeda)
            {
                onesposobi_buttons();
                String pobjednik = "";
                if (naredu)
                {
                    pobjednik = igrac2;
                    label7.Text = (Int32.Parse(label7.Text) + 1).ToString();


                }
                else
                {
                    pobjednik = igrac1;
                    label8.Text = (Int32.Parse(label8.Text) + 1).ToString();


                }

                MessageBox.Show(pobjednik + " je pobijedio / la!!");

            }
            else if (br == 9)
            {

                MessageBox.Show("Neriješeno!!");
            }

        }

        private void onesposobi_buttons()
        {
            try
            {
                foreach (Control control in Controls)
                {

                    Button button = (Button)control;
                    button.Enabled = false;

                }
            }
            catch { }


        }

        private void pokreni1_Click(object sender, EventArgs e)
        {
            Form1.Postaviime(textbox1.Text, textbox2.Text);
            label5.Text = igrac1;
            label4.Text = igrac2;
            label.Text = igrac1;
        }

       





    }
}
